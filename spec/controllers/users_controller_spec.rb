require 'rails_helper'

RSpec.describe UsersController, type: :controller do

	let(:valid_attributes) {{
    name: 'Test User',
   	email: 'test@yopmail.com',
   	password: '12345678',
   	password_confirmation: '12345678',
   	roles: ["supervisor", "admin"]
  }}

  let(:valid_attributes_without_roles) {{
    name: 'Test User',
   	email: 'test@yopmail.com',
   	password: '12345678',
   	password_confirmation: '12345678'
  }}

  let(:invalid_attributes) {{
  	name: 'Test User',
		email: '',
		password: '1234',
		password_confirmation: '1234',
		roles: ["caller"]
  }}
	
  before do
    @user = create(:user)
    # @user.add_role(:super_admin)
    sign_in @user
  end

	describe 'GET #index' do
		before { get :index }
		it { should respond_with 200 }
	end

	describe 'GET #new' do
		it "returns a success response" do
			get :new, params: {}
			expect(response).to be_successful
		end
	end

	describe 'POST #create' do
		context "with valid params" do
			it 'create a new user' do
			  expect { post(:create, params: { user: valid_attributes }) }.to change(User, :count).by(1)
			end

			it "redirects to the users list" do
        post :create, params: {user: valid_attributes}
        expect(response).to redirect_to(employees_path)
      end
		end

		context "with invalid params" do
			it "returns a success response (i.e to display the 'new' template)" do
				post :create, params: {user: invalid_attributes}
				expect(response).to be_successful
			end
		end
	end

	describe 'GET #edit' do
		it 'returns a success response' do
			user = User.create! valid_attributes_without_roles
			user.add_role(:supervisor)
			get :edit, params: {id: user.to_param}
			expect(response).to be_successful
		end
	end

	describe 'PUT #update' do
		context "with valid params" do
			let(:new_attributes) {{
				name: 'Test User 1',
		   	email: 'test1@yopmail.com',
		   	roles: ["caller"]
			}}

			it "updates the requested user" do
				user = User.create! valid_attributes_without_roles
				user.add_role(:supervisor)
				put :update, params: {id: user.to_param, user: new_attributes}
				user.reload
				new_attributes.keys.each do |key|
					if key.to_s == "roles"
						expect(user.roles.map(&:name)).to eq new_attributes[key]
					else
			    	expect(user.attributes[key.to_s]).to eq new_attributes[key]
			    end
			  end
			end

			it "redirects to users list" do
				user = User.create! valid_attributes_without_roles
				user.add_role(:supervisor)
				put :update, params: {id: user.to_param, user: new_attributes}
				expect(response).to redirect_to(employees_path)
			end
		end

		context "with invalid params" do
			let(:new_invalid_attributes) {{
				name: 'Test User 1',
		   	email: 'test1',
		   	roles: ["caller"]
			}}

			it "returns a success response (i.e. to display the 'edit' template)" do
				user = User.create! valid_attributes_without_roles
				user.add_role(:supervisor)
				put :update, params: {id: user.to_param, user: new_invalid_attributes}
				expect(response).to be_successful
			end
		end
	end

	describe 'DELETE #destroy' do
		it 'destroys the requested user' do
			user = User.create! valid_attributes_without_roles
			user.add_role(:supervisor)
			expect{ delete :destroy, params: {id: user.to_param} }.to change(User, :count).by(-1)
		end

		it "redirects to users list" do
			user = User.create! valid_attributes_without_roles
			user.add_role(:supervisor)
			delete :destroy, params: {id: user.to_param}
			expect(response).to redirect_to(employees_path)
		end
	end
end
