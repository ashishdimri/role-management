require 'rails_helper'

RSpec.describe HomeController, type: :controller do
	describe 'GET #index' do
		context 'without user signed in' do
			before { get :index }
			it { redirect_to(new_user_session_path) }
		end

		context 'with user signed in' do
			before do
				@user = create(:user)
		    # @user.add_role(:super_admin)
		    sign_in @user
		    get :index
			end

			it { should respond_with 200 }
		end
	end
end
