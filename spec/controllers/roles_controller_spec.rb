require 'rails_helper'

RSpec.describe RolesController, type: :controller do

	before do
		@user = create(:user)
    # @user.add_role(:super_admin)
    sign_in @user
	end

	describe 'GET #index' do
		before { get :index }
		it { should respond_with 200 }
	end

	describe 'GET #new' do
		it 'returns a success response' do
			get :new, params: {}
			expect(response).to be_successful
		end
	end

	describe 'POST #create' do
		context "with valid params" do
			it 'creates a new Role' do
				expect{
					post :create, params: {role: {name: "admin"}}
				}.to change(Role, :count).by(1)
			end

			it 'redirects to the roles list' do
				post :create, params: {role: {name: "supervisor"}}
				expect(response).to redirect_to(roles_path)
			end
		end

		context "with invalid params" do
			it "returns a success response (i.e to display the 'new' template)" do
				post :create, params: {role: {name: ""}}
				expect(response).to be_successful
			end
		end
	end

	describe 'GET #edit' do
		it 'returns a success response' do
			role = Role.create(name: "admin")
			get :edit, params: {id: role.to_param}
			expect(response).to be_successful
		end
	end

	describe 'PUT #update' do
		context "with valid params" do
			let(:new_attributes){
				{name: "supervisor"}
			}
			it "updates a requested role" do
				role = Role.create(name: "admin")
				put :update, params: {id: role.to_param, role: new_attributes}
				role.reload
				new_attributes.keys.each do |key|
			    expect(role.attributes[key.to_s]).to eq new_attributes[key]
			  end
			end

			it "redirects to the roles list" do
				role = Role.create(name: "admin")
				put :update, params: {id: role.to_param, role: new_attributes}
				expect(response).to redirect_to(roles_path)
			end
		end

		context "with invalid params" do
			let(:new_invalid_attributes){
				{name: ""}
			}

			it "returns a success response (i.e. to display the 'edit' template)" do
				role = Role.create(name: "admin")
				put :update, params: {id: role.to_param, role: new_invalid_attributes}
				expect(response).to be_successful
			end
		end
	end

	describe 'DELETE #destroy' do
		before{
			@role = Role.create(name: "admin")
		}
		it "destroy a requested role" do
			expect{
				delete :destroy, params: {id: @role.to_param}
			}.to change(Role, :count).by(-1)
		end

		it "redirects to roles list" do
			delete :destroy, params: {id: @role.to_param}
			expect(response).to redirect_to(roles_path)
		end
	end

	describe "GET #fetch_roles" do
		before do
			role = Role.create(name: "admin")
			get :fetch_roles, params: {q: "a"}
		end
		it { should respond_with 200 }
	end
end
