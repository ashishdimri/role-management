require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
	before do
    @user = create(:user)
    # @user.add_role(:super_admin)
    sign_in @user
  end

  describe 'GET #index' do
  	before { get :index }
  	it {should respond_with 200}
  end

  describe 'GET #new' do
  	it 'returns a success response' do
  		get :new, params: {}
  		expect(response).to be_successful
  	end
  end

  describe 'POST #create' do
  	context 'with valid params' do
  		it 'creates a new product' do
  			expect{
  				post :create, params: {product: {name: "Perfume", description: "Perfume for men", price: 1000}}
  			}.to change(Product, :count).by(1)
  		end

  		it 'redirects to products path' do
  			post :create, params: {product: {name: "Perfume", description: "Perfume for men", price: 1000}}
  			expect(response).to redirect_to(products_path)
  		end
  	end

  	context 'with invalid params' do
  		it 'returns a success response (i.e to display a "new" template)' do
  			post :create, params: {product: {name: "", description: "", price: nil}}
  			expect(response).to be_successful
  		end
  	end
  end

  describe 'GET #edit' do
  	it 'returns a success response' do
  		product = Product.create(name: "Perfume", description: "Perfume for men", price: 1000)
  		get :edit, params: {id: product.to_param}
  		expect(response).to be_successful
  	end
  end

  describe 'PUT #update' do
  	context 'with valid params' do
  		let(:new_attributes){
	  		{name: "Perfume", description: "Perfume for women", price: 1200}
	  	}
  		it 'updates a requested product' do
  			product = Product.create(name: "Perfume", description: "Perfume for men", price: 1000)
  			put :update, params:{id: product.to_param, product: new_attributes}
  			product.reload
  			new_attributes.keys.each do |key|
			    expect(product.attributes[key.to_s]).to eq new_attributes[key]
			  end
  		end

  		it 'redirects to products list' do
  			product = Product.create(name: "Perfume", description: "Perfume for men", price: 1000)
  			put :update, params:{id: product.to_param, product: new_attributes}
  			expect(response).to redirect_to(products_path)
  		end
  	end

  	context 'with invalid params' do
  		let(:new_invalid_attributes){
	  		{name: "", description: "", price: nil}
	  	}
	  	it 'returns a success response (i.e to display "edit" template)' do
	  		product = Product.create(name: "Perfume", description: "Perfume for men", price: 1000)
  			put :update, params:{id: product.to_param, product: new_invalid_attributes}
  			expect(response).to be_successful
	  	end
  	end
  end

  describe 'DELETE #destroy' do
  	before{
  		@product = Product.create(name: "Perfume", description: "Perfume for men", price: 1000)
  	}
  	it 'destroys the requested product' do
  		expect{
	  		delete :destroy, params: {id: @product.to_param}
  		}.to change(Product, :count).by(-1)

  	end

  	it 'redirects to products list' do
  		delete :destroy, params: {id: @product.to_param}
  		expect(response).to redirect_to(products_path)
  	end
  end
end
