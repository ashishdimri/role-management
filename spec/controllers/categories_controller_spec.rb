require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
	before do
    @user = create(:user)
    # @user.add_role(:super_admin)
    sign_in @user
  end

  describe 'GET #index' do
  	before { get :index }
  	it { should respond_with 200 }
  end

  describe 'GET #new' do
  	it 'return a success response' do
  		get :new, params: {}
  		expect(response).to be_successful
  	end
  end

  describe 'POST #create' do
  	context 'with valid params' do
  		it 'creates a new category' do
  			expect{
  				post :create, params: {category: {name: "Men", description: "Men's Category"}}
  			}.to change(Category, :count).by(1)
  		end

  		it 'redirects to categories list' do
  			post :create, params: {category: {name: "Men", description: "Men's Category"}}
  			expect(response).to redirect_to(categories_path)
  		end
  	end

  	context 'with invalid params' do
  		it "returns a success response (i.e to display the 'new' template)" do
  			post :create, params: {category: {name: "", description: ""}}
  			expect(response).to be_successful
  		end
  	end
  end

  describe 'GET #edit' do
  	it 'returns a success response' do
  		category = Category.create(name: "Men", description: "Men's Category")
  		get :edit, params: {id: category.to_param}
  		expect(response).to be_successful
  	end
  end

  describe 'PUT #update' do
  	context 'with valid params' do
  		let(:new_attributes){
  			{name: "Women",
  			description: "Women's Category"}
  		}
  		it 'updates a requested category' do
  			category = Category.create(name: "Men", description: "Men's Category")
  			put :update, params: {id: category.to_param, category: new_attributes }
  			category.reload
  			new_attributes.keys.each do |key|
			    expect(category.attributes[key.to_s]).to eq new_attributes[key]
			  end
  		end

  		it 'redirects to categories list' do
  			category = Category.create(name: "Men", description: "Men's Category")
  			put :update, params: {id: category.to_param, category: new_attributes }
  			expect(response).to redirect_to(categories_path)
  		end
  	end

  	context 'with invalid params' do
  		let(:new_invalid_attributes){
  			{name: "",
  			description: ""}
  		}
  		it 'returns a success response (i.e to display "edit" template)' do
  			category = Category.create(name: "Men", description: "Men's Category")
  			put :update, params: {id: category.to_param, category: new_invalid_attributes }
  			expect(response).to be_successful
  		end
  	end
  end

  describe 'DELETE #destroy' do
  	before{
			@category = Category.create(name: "Men", description: "Men's Category")
		}
		it "destroys a requested category" do
			expect{
				delete :destroy, params: {id: @category.to_param}
			}.to change(Category, :count).by(-1)
		end

		it "redirects to categories list" do
			delete :destroy, params: {id: @category.to_param}
			expect(response).to redirect_to(categories_path)
		end
  end
end
