FactoryGirl.define do
  factory :user do
  	name     							{ FFaker::Name.name }
    email    							{ FFaker::Internet.email }
    password 							"12345678"
    password_confirmation "12345678"
  end

  factory :confirmed_user, :parent => :user do
    after(:create) { |user| user.confirm! }
  end
end