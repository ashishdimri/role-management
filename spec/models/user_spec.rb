require 'rails_helper'

RSpec.describe User, type: :model do
	before {@user = build(:user)}
  
  subject{@user}

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:reset_password_token) }

  it { should be_valid }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:email) }
  it { should validate_uniqueness_of(:email).ignoring_case_sensitivity }
  it { should validate_confirmation_of(:password) }
  it { should allow_value('example@example.com').for(:email) }

  it { should have_and_belong_to_many(:roles) }
  it { should have_and_belong_to_many(:permissions) }
end
