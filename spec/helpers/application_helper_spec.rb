require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the CategoriesHelper. For example:
#
# describe CategoriesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end

RSpec.describe ApplicationHelper, type: :helper do
  describe "#remove_trailing_comma" do
  	it 'removes extra space and comma from a string' do
  		string_with_comma = "Hello, World, "
  		after_string = helper.send(:remove_trailing_comma, string_with_comma)
  		expect(after_string).to eq("Hello, World")
  	end
  end

  describe '#check_presence' do
		before { @category = Category.create(name: "Men") }
  	context 'without empty object/string' do
	  	it 'returns object/string if it is present' do
	  		present = helper.send(:check_presence, @category.name)
	  		expect(present).to eq(@category.name)
	  	end
	  end

	  context 'with empty object/string' do
	  	it 'returns empty sign it object/string is not present' do
	  		not_present = helper.send(:check_presence, @category.description)
	  		expect(not_present).to eq("-")
	  	end
	  end
  end

  describe '#make_string_camel_case' do
  	it 'returns CamelCase string with spaces' do
  		camel_case_string = "HelloWorld"
  		after_string = helper.send(:make_string_camel_case, camel_case_string)
  		expect(after_string).to eq("Hello World")
  	end
  end
end