require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the UsersHelper. For example:
#
# describe UsersHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe UsersHelper, type: :helper do
	before { @user = FactoryGirl.build(:user) }
  describe "#get_roles" do
    before do
      @user.save
      @user.add_role(:super_admin)
      @user.add_role(:admin)
      @user.add_role(:supervisor)
    end

    it "returns all the roles of a user" do
      # roles = helper.remove_trailing_comma(helper.get_roles(@user))
      roles = helper.send(:remove_trailing_comma, helper.get_roles(@user))
      # roles = ""
      # @user.roles.each do |role|
      # 	roles += role.name + ", "
      # end
      expect(roles).to eq("super_admin, admin, supervisor")
    end
  end
end
