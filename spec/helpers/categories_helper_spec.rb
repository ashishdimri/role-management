require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the CategoriesHelper. For example:
#
# describe CategoriesHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe CategoriesHelper, type: :helper do
  before do
  	@category1 = Category.create(name: "Men", description: "Men's Category")
  	@category2 = Category.create(name: "Top", description: "Men's Top", parent_id: @category1.id)
  	# @category3 = Category.create(name: "Bottom", description: "Men's Bottom", parent_id: @category1.to_param)
  	# @category4 = Category.create(name: "Shirt", description: "Men's Shirt", parent_id: @category2.to_param)
  	# @category5 = Category.create(name: "Jeans", description: "Men's Jeans", parent_id: @category3.to_param)
  end

  describe '#get_parent_category' do
  	it 'returns the parent category name' do
  		parent_category = helper.get_parent_category(@category2)
  		expect(parent_category).to eq("Men")
  	end
  end

  describe '#get_select_categories' do
  	it 'returns array of all categories' do
  		category_array = helper.get_select_categories
  		expect(category_array).to eq([["-- none --", nil], ["Men", @category1.id], ["-Top", @category2.id]])
  	end
  end

  describe '#get_select_categories_without_selected' do
  	it 'returns array of all categories except the category passed as parameter' do
  		category_array = helper.get_select_categories_without_selected(@category2)
  		expect(category_array).to eq([["-- none --", nil], ["Men", @category1.id]])
  	end
  end
end
