require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ProductsHelper. For example:
#
# describe ProductsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ProductsHelper, type: :helper do
  before do
  	@category1 = Category.create(name: "Men", description: "Men's Category")
  	@category2 = Category.create(name: "Top", description: "Men's Top", parent_id: @category1.id)
  	
  	@product1 = Product.create(name: "aacdcsd", description: "ddccd", price: "1110.0", category_ids: [@category1.to_param, @category2.to_param])
  end

  describe '#get_product_categories' do
  	it 'returns the product categories' do
  		product_categories = helper.send(:remove_trailing_comma, helper.get_product_categories(@product1))
  		expect(product_categories).to eq("Men, Top")
  	end
  end
end