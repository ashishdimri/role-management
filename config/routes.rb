Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

	devise_scope :user do
	  authenticated :user do
	    root 'home#index', as: :authenticated_root
	  end

	  unauthenticated do
	    root 'devise/sessions#new', as: :unauthenticated_root
	  end
	end

  scope '/admin' do
    # Home Controller Starts
    get '/home/index', to: 'home#index'
    # Home Controller Ends

    # Users Controller Starts
    get '/employees', to: 'users#index'
    post '/employees', to: 'users#create'
    get '/employees/new', to: 'users#new', as: 'new_employee'
    get '/employees/:id/edit', to: 'users#edit', as: 'edit_employee'
    get '/employees/:id', to: 'users#show', as: 'employee'
    patch '/employees/:id', to: 'users#update'
    put 'employees/:id', to: 'users#update'
    delete 'employees/:id', to: 'users#destroy'

    get '/fetch_roles', to: 'roles#fetch_roles'
    # Users Controller Ends

    resources :roles, except: [:show]
    resources :categories, except: [:show]
    resources :products, except: [:show]
  end
  
  get '*page', :to => 'errors#not_found', as: :not_found
  get '*page', :to => 'errors#unacceptable', as: :unacceptable
  get '*page', :to => 'errors#internal_error', as: :internal_error
end
