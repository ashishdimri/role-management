class CreateUsersPermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :users_permissions do |t|
      t.references :user, foreign_key: true
      t.references :permission, foreign_key: true
    end
  end
end
