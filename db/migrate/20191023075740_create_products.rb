class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
    	t.string :name,               null: false, default: ""
    	t.string :description,        null: false, default: ""
    	t.decimal :price,             null: false, precision: 10, scale: 2, default: 0.00
    	
      t.timestamps
    end
  end
end
