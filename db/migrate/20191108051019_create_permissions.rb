class CreatePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :permissions do |t|
    	t.string :name,               null: false
    	t.string :controller_type,    null: false
      t.timestamps
    end

    add_index :permissions, :name,                unique: true
    add_index :permissions, :controller_type
  end
end
