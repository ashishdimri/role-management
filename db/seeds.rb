# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Rails.application.eager_load!
controllers = ApplicationController.descendants

controllers.each do |con|
  con_str = con.to_s
  next if con_str == "DeviseController" || con_str == "ErrorsController" || con_str == "HomeController"
  con_actions = con.action_methods.to_a
  con_actions.each do |ac|
    permission_name = con_str.gsub("Controller", "").downcase + "#" + ac
    Permission.find_or_create_by(name: permission_name, controller_type: con_str)
  end
end