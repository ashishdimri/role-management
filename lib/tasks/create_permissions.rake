namespace :permission do
	desc "Create permissions"
  task :create_permissions => :environment do
    Rails.application.eager_load!
    controllers = ApplicationController.descendants

    controllers.each do |con|
    	next if NEGLECT_CONTROLLERS.include?(con.to_s)
    	con.action_methods.to_a.each do |action_method|
    		p_name = con.to_s.gsub("Controller", "").downcase + "#" + action_method
    		Permission.find_or_create_by(name: p_name, controller_type: con.to_s)
    	end
    end
  end

  private

  	NEGLECT_CONTROLLERS = ["DeviseController", "Devise::OmniauthCallbacksController", "Users::OmniauthCallbacksController", "ErrorsController"]
end