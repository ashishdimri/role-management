module ApplicationHelper
	private

	def remove_trailing_comma(str)
    str.nil? ? nil : str.chomp(", ")
	end

	def check_presence(object)
		object.present? ? object : "-"
	end

	# def get_controllers
 #    controller_actions = []
 #    Rails.application.eager_load!
 #    controllers = ApplicationController.descendants

 #    controllers.each do |con|
 #    	next if con.to_s == "DeviseController"
 #      controller_actions.push(con.action_methods.to_a.map{|ac| con.to_s.gsub("Controller", "").downcase + "#" + ac})
 #    end
 #    controller_actions.flatten
 #  end

  def make_string_camel_case(str)
    str_array = str.split /(?=[A-Z])/
    result_str = str_array.join(' ')
  end

  def have_permission(path, path_method=nil)
  	if !current_user.has_role?(:super_admin)
	  	path_description = path_method.present? ? Rails.application.routes.recognize_path(path, method: path_method) : Rails.application.routes.recognize_path(path)
	  	controller_action = path_description[:controller] + "#" + path_description[:action]
	  	if current_user.permissions.map(&:name).include?(controller_action)
	  		return true
	  	else
	  		return false
	  	end
	  else
	  	return true
	  end
  end
end
