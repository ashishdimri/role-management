module ProductsHelper
	def get_product_categories(product)
		categories = ""
		if product.categories.present?
			product.categories.each do |category|
				categories += category.name + ", "
			end
			return categories
		end
	end
end
