module CategoriesHelper
	def get_parent_category(category)
		if category.parent.present?
			return category.parent.name
		end
	end

	def get_select_categories
		Category.all.each { |c| c.ancestry = c.ancestry.to_s + (c.ancestry != nil ? "/" : '') + c.id.to_s 
      }.sort {|x,y| x.ancestry <=> y.ancestry 
      }.map{ |c| ["-" * (c.depth - 1) + c.name,c.id] 
      }.unshift(["-- none --", nil])
	end

	def get_select_categories_without_selected(cat)
		Category.all.each { |c| c.ancestry = c.ancestry.to_s + (c.ancestry != nil ? "/" : '') + c.id.to_s 
      }.reject{|k| k.name == cat.name
      }.sort {|x,y| x.ancestry <=> y.ancestry 
      }.map{ |c| ["-" * (c.depth - 1) + c.name,c.id] 
      }.unshift(["-- none --", nil])
	end
end
