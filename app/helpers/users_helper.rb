module UsersHelper
	def get_roles(user)
		roles = ""
		if user.roles.present?
			user.roles.each do |role|
				roles += role.name + ", "
			end
			return roles
		end
	end
end
