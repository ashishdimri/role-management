class ErrorsController < ApplicationController

  layout :set_layout_errors

	def not_found
    render :status => 404
  end

  def unacceptable
    render :status => 422
  end

  def internal_error
    render :status => 500
  end

  # def permission_denied
  # 	render :status => 403
  # end

  private

    def set_layout_errors
      if user_signed_in?
        'application'
      else
        'auth'
      end
    end
end
