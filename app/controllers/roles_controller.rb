class RolesController < ApplicationController
	before_action :authenticate_user!
	before_action :set_role, only: [:edit, :update, :destroy]

	def index
		@roles = Role.all
	end

	def new
		@role = Role.new
	end

	def create
		role = Role.new(roles_params)
		if role.save
			respond_to do |format|
				format.html { redirect_to roles_path, notice: 'Role was successfully created.' }
	      format.json { render json: {notice: 'Role was successfully created.'}, status: 200 }
	    end
		else
			respond_to do |format|
				format.html { render :new }
	      format.json { render json: {error: role.errors}, status: :unprocessable_entity }
	    end
		end
	end

	def edit
	end

	def update
		if @role.update_attributes(roles_params)
			redirect_to roles_path, notice: 'Role was successfully updated.'
		else
			render 'edit'
		end
	end

	def destroy
		@role.destroy
		redirect_to roles_path, notice: 'Role was successfully destroyed.'
	end

	def fetch_roles
		@roles = Role.where("name LIKE ?", "%#{params[:q]}%")
		render json: @roles, status: 200
	end


	private

		def roles_params
			params.require(:role).permit(:name)
		end

		def set_role
			@role = Role.find_by(id: params[:id])
		end
end
