class UsersController < ApplicationController
	before_action :authenticate_user!
	before_action :set_params, only: [:create]
	before_action :set_user, only: [:show, :edit, :update, :destroy]
	before_action :get_controllers, only: [:new, :edit]

	def index
		@users = User.includes(:roles)
	end

	def show
	end

	def new
		@user = User.new
		@user.roles.build
		@user.permissions.build
	end

	def create
		generated_password = @passwrd
		roles = []
		roles = params[:user][:roles].compact
		roles = roles.reject { |e| e.to_s.empty? }
		user = User.new(user_params)
		if user.save
			# Save user credentials to file Starts (Only for testing purpose)
			save_user_cred(generated_password)
			# Save user credentials to file End
			add_user_roles(user, roles)

			redirect_to employees_path, notice: 'User is successfully created.'
		else
			render 'new'
		end
	end

	def edit
		begin
			if !@user.roles.present?
				@user.roles.build
			end
		rescue Exception => e
		end
	end

	def update
		roles = []
		roles = params[:user][:roles].compact
		roles = roles.reject { |e| e.to_s.empty? }
		if @user.update_attributes(user_update_params)
			@user.roles.each do |role|
	      @user.remove_role(role.name.to_sym)
	    end
	    add_user_roles(@user, roles)
	    redirect_to employees_path, notice: 'User is successfully updated.'
		else
			render 'edit'
		end
	end

	def destroy
		@user.destroy
		redirect_to employees_path, notice: 'User is successfully d: []estroyed.'
	end

	private

		def user_params
			params.require(:user).permit(:name, :email, :password, :password_confirmation, permission_ids: [])
		end

		def user_update_params
			params.require(:user).permit(:name, :email, permission_ids: [])
		end

		def set_params
			@passwrd = Devise.friendly_token.first(8)
			params[:user][:password] = @passwrd
			params[:user][:password_confirmation] = @passwrd
		end

		def set_user
			@user = User.find_by(id: params[:id])
		end

		def add_user_roles(user, roles)
			roles.each do |role|
				user.add_role(role.to_sym)
			end
		end

		def save_user_cred(pass)
			user_cred = {}
			user_cred[:email]=params[:user][:email]
			user_cred[:password]=pass
			file = File.open(Rails.public_path.join('user_cred.txt'), "a")
			file.puts user_cred.to_s
			file.close
		end

		def get_controllers
			@controller_types = Permission.all.map(&:controller_type).uniq
		end
end
