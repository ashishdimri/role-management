class ApplicationController < ActionController::Base
  include ApplicationHelper
	protect_from_forgery with: :exception
	before_action :configure_permitted_parameters, if: :devise_controller?
  # before_action :get_controllers, if: :user_signed_in?
  before_action :check_permissions, if: :user_signed_in?
  # before_action :check_errors
	layout :set_layout

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    end

  private

    def set_layout
    	if devise_controller?
    		'auth'
    	else
    		'application'
    	end
    end

    def check_permissions
      recognize_path = Rails.application.routes.recognize_path(request.path) rescue ""
      if !current_user.has_role?(:super_admin) && !(controller_path == "errors") && !(controller_path == "home") && !devise_controller?
        all_permissions = current_user.permissions.map(&:name)
        goto_controller = controller_path + "#" + action_name
        if !all_permissions.include?(goto_controller)
          render_permission_denied
        end
      end
    end

    def render_permission_denied
      if request.format == "application/json"
        render json: {status: 403}
      else
        render :file => "#{Rails.root}/public/403.html",  :status => 403
      end
    end

    # def check_errors
    #   debugger
    #   if controller_path == "errors" && action_name == "not_found"
    #     redirect_to(not_found_path)
    #   end
    # end
end
