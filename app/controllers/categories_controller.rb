class CategoriesController < ApplicationController
	before_action :authenticate_user!
	before_action :set_category, only: [:edit, :update, :destroy]

	def index
		begin
			@categories = Category.all
		rescue Exception => e
			
		end
	end

	def new
		@category = Category.new
	end

	def create
		@category = Category.new(category_params)
		respond_to do |format|
      if @category.save
        format.html { redirect_to categories_path, notice: 'Category was successfully created.' }
        format.json { render action: 'index', status: :created, location: @category }
      else
        format.html { render action: 'new' }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
	end

	def edit
		
	end

	def update
		if @category.update_attributes(category_params)
			redirect_to categories_path, notice: "Category was successfully updated."
		else
			render 'edit'
		end
	end

	def destroy
		@category.destroy
		redirect_to categories_path, notice: "Category is successfully destroy"
	end

	private

		def set_category
			@category = Category.find_by(id: params[:id])
		end

		def category_params
			params.require(:category).permit(:name, :description, :parent_id)
		end
end
