class ProductsController < ApplicationController
	before_action :authenticate_user!
	before_action :set_product, only: [:edit, :update, :destroy]

	def index
		@products = Product.includes(:categories)
	end

	def new
		@product = Product.new
	end

	def create
		@product = Product.new(product_params)
		respond_to do |format|
      if @product.save
        format.html { redirect_to products_path, notice: 'Product was successfully created.' }
        format.json { render action: 'index', status: :created, location: @product }
      else
        format.html { render action: 'new' }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
	end

	def edit

	end

	def update
		if @product.update_attributes(product_params)
			redirect_to products_path, notice: "Product was successfully updated."
		else
			render 'edit'
		end
	end

	def destroy
		@product.destroy
		redirect_to products_path, notice: 'Product was successfully destroyed.'
	end

	private

		def product_params
			params.require(:product).permit(:name, :description, :price, category_ids: [])
		end

		def set_product
			@product = Product.find_by(id: params[:id])
		end
end
