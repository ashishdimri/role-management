class User < ApplicationRecord
  rolify
  has_and_belongs_to_many :permissions, :join_table => :users_permissions
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  after_create :assign_default_role

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2]

	validates_presence_of :name, :email

  accepts_nested_attributes_for :roles

  accepts_nested_attributes_for :permissions
  
  def assign_default_role
  	users = User.all
  	if users.count == 1
  		add_role(:super_admin) if self.roles.blank?
  	# else
  	# 	add_role(:normal) if self.roles.blank?
  	end
  end

  def self.from_omniauth(auth)
    # data = access_token.info
    # user = User.where(email: data['email']).first

    # Uncomment the section below if you want users to be created if they don't exist
    # unless user
    #     user = User.create(name: data['name'],
    #        email: data['email'],
    #        password: Devise.friendly_token[0,20]
    #     )
    # end
    # user
    debugger
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
    end
  end

  # def self.find_or_create_from_auth_hash(auth)
  #   where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |user|
  #     user.provider = auth.provider
  #     user.uid = auth.uid
  #     user.first_name = auth.info.first_name
  #     user.last_name = auth.info.last_name
  #     user.email = auth.info.email
  #     user.picture = auth.info.image
  #     user.save!
  #   end
  # end

  # def self.from_omniauth(auth)
  #   # Either create a User record or update it based on the provider (Google) and the UID   
  #   where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
  #     user.token = auth.credentials.token
  #     user.expires = auth.credentials.expires
  #     user.expires_at = auth.credentials.expires_at
  #     user.refresh_token = auth.credentials.refresh_token
  #   end
  # end
end
