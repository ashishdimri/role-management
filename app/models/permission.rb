class Permission < ApplicationRecord
	has_and_belongs_to_many :users, :join_table => :users_permissions
	accepts_nested_attributes_for :users
end
