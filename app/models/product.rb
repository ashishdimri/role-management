class Product < ApplicationRecord
	validates_presence_of :name, :description, :price
	has_and_belongs_to_many :categories, :join_table => :categories_products
end
