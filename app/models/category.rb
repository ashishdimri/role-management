class Category < ApplicationRecord
	has_ancestry
	has_and_belongs_to_many :products, :join_table => :categories_products
	validates_presence_of :name
end
